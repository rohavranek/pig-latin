<?php
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../PigLatin/PigLatin.php';

use PigLatin\PigLatin;
use Tester\Assert;

Tester\Environment::setup();


// Here are tests for words that do not contain the vowel in the first position
Assert::same('igpay', PigLatin::convert('pig'));
Assert::same('atinlay', PigLatin::convert('latin'));
Assert::same('ananabay', PigLatin::convert('banana'));
Assert::same('appyhay', PigLatin::convert('happy'));
Assert::same('uckday', PigLatin::convert('duck'));

//Here are tests for words that contain a vowel in the first position
Assert::same('eatay', PigLatin::convert('eat'));
Assert::same('omeletay', PigLatin::convert('omelet'));
Assert::same('areay', PigLatin::convert('are'));
Assert::same('eggay', PigLatin::convert('egg'));
Assert::same('explainay', PigLatin::convert('explain'));

//Here are tests for words that contain a consonant cluster in the first position
Assert::same('ilesmay', PigLatin::convert('smile'));
Assert::same('oorflay', PigLatin::convert('floor'));
Assert::same('upidstay', PigLatin::convert('stupid'));
Assert::same('oveglay', PigLatin::convert('glove'));
Assert::same('ashtray', PigLatin::convert('trash'));
