[PigLatin](https://en.wikipedia.org/wiki/Pig_Latin) - language game
================================================================

Introduction
------------

Pig Latin is a language game or argot in which words in English are altered, usually by adding a fabricated suffix or by moving the onset or initial consonant or consonant cluster of a word to the end of the word and adding a vocalic syllable to create such a suffix.

Pig Latin Two rules:
- **Rule 1**: If a word begins with a vowel sound, add an "ay" sound to
  the end of the word.
- **Rule 2**: If a word begins with a consonant sound, move it to the
  end of the word, and then add an "ay" sound to the end of the word.

There are a few more rules for edge cases, and there are regional
variants too.

More: <http://en.wikipedia.org/wiki/Pig_latin> 

Examples: 
```
"pig" = "igpay"
"latin" = "atinlay"
"eat" = "eatay"
```

How to run project
------------

You need the composer and Docker installed to run this project.


First install dependencies:
```
$ composer install
```

Run conteinters - by default project will be available at localhost:81 
(you can edit port in docker-compose.yml)

```
$ docker-compose up
```

Now project is available at localhost:81

How to run tests
------------
```
$ cd tests
$ php piglatin_tests.php
```
