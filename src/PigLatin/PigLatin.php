<?php
declare(strict_types=1);

namespace PigLatin;

class PigLatin
{

	private const VOWELS = [
		'a', 'e', 'i', 'o', 'u'
	];

	private const SUFFIX = 'ay';

	private const PUNCTUATION = [
		',', '.', ':', ';'
	];

	/**
	 * @param string $string
	 * @return array
	 */
	public static function convert(string $string): string
	{
		$words = explode(' ', $string);

		return implode(' ' , self::checkPunctuation(array_map('self::translateWord', $words)));
	}

	/**
	 * @param string $word
	 * @return string
	 */
	private static function translateWord(string $word): string
	{
		if (in_array($word[0], self::VOWELS, true)) {
			return $word . self::SUFFIX;
		}

		if (!in_array($word[0], self::VOWELS, true) && !in_array($word[1], self::VOWELS, true)) {
			return self::replaceConsonantCluster($word);
		}

		return self::replaceConsonant($word);
	}

	/**
	 * @param string $string
	 * @return string
	 */
	private static function replaceConsonant(string $string): string
	{
		return substr($string, 1) . $string[0] . self::SUFFIX;
	}

	/**
	 * @param string $string
	 * @return string
	 */
	private static function replaceConsonantCluster(string $string): string
	{
		return substr($string, 2) . $string[0] . $string[1] . self::SUFFIX;
	}

	/**
	 * @param array $words
	 * @return array
	 */
	private static function checkPunctuation(array $words): array
	{
		/** @var string $word */
		foreach ($words as $key => $word)
		{
			/** @var string $item */
			foreach (self::PUNCTUATION as $item)
			{
				$position = strpos($word, $item);
				if ($position)
				{
					$formattedString = str_replace($item, '', $word);
					$words[$key] = $formattedString . $item;
				}
			}
		}

		return $words;
	}
}
